package com.greatlearning.Swagger.service;

import com.greatlearning.Swagger.dao.CourseRepository;
import com.greatlearning.Swagger.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public String saveCourse(Course course){
        courseRepository.save(course);
        return "saved course with id"+course.getCourseId();
    }

    public List<Course> getAllCourse(){
        return courseRepository.findAll();
    }

    public List<Course> deleteCourseById(int courseId){
        courseRepository.deleteById(courseId);
        return courseRepository.findAll();
    }


}
