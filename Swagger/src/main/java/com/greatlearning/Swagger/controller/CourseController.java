package com.greatlearning.Swagger.controller;

import com.greatlearning.Swagger.entity.Course;
import com.greatlearning.Swagger.service.CourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @PostMapping("/save")
    @ApiOperation(value="it is saving new course")
    public String saveCourse(Course course){
        return courseService.saveCourse(course);
    }

    @GetMapping("/getCourse")
    @ApiOperation(value="it will fetch all courses")
    public List<Course> getAllCourses(){
        return courseService.getAllCourse();
    }

    @DeleteMapping("/deleteCourse/{courseId}")
    @ApiOperation(value= "it will delete course with id")
    public List<Course> deleteCourseById(int courseId){
        return courseService.deleteCourseById(courseId);
    }

}
